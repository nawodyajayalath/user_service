package com.userservice.UserService.services;

import com.userservice.UserService.dtos.OrderDTO;
import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.entities.UserEntity;
import com.userservice.UserService.repositories.UserRepository;
import com.userservice.UserService.utils.Validations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    public List<OrderDTO> getOrderesByUserId(Long id){
        LOGGER.info("=============== URI " + orderServiceBaseUrl + orderServiceOrderUrl + "/1 ========================");
        List<OrderDTO> orders = null;
        try{
            orders = restTemplateBuilder.build().getForObject(
                    orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/" + id),
                    List.class
            );
        }catch (Exception e){
            System.out.println("Exception Rest Template Builder Error: " + e);
        }
        return orders;
    }

    public List<UserDTO> getAllUsers(){
        LOGGER.info("/========== ENTER INTO getUserService in UserService =========================/");
        List<UserDTO> users = null;
        try {
            users = userRepository.findAll()
                    .stream()
                    .map(userEntity -> new UserDTO(
                            userEntity.getId(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )).collect(Collectors.toList());
        } catch (Exception e){
            System.out.println("Exception: " + e);
        }

        return users;
    }

    /**
     * ========================================
     * Save user after validate
     * ====================================
     * @param user userDTO
     *
     */
    public boolean saveUser(UserDTO user){
        LOGGER.info("=========== In the save method =================");
        try{
            if(Validations.validateUser(user)){
                UserEntity userEntity = new UserEntity(user.getName(), user.getAge());
                userRepository.save(userEntity);
            } else{
                LOGGER.info("========== Validation Failed ===============");
                return false;
            }
        } catch (Exception e){
            LOGGER.info("=============== Error in User Save ============== " + e.getMessage());
        }
        return false;
    }

    /**
     * =========================================
     * Method used to update existing user
     * ========================================
     * @param user
     * @return
     */
    public ResponseEntity<String> updateUser(UserDTO user){
        LOGGER.info("=================== Entered into updateUser method in UserService =========");

        if(user.getId() == null){
            return new ResponseEntity<>("ID can't be empty ", HttpStatus.BAD_REQUEST);
        }
        try{
            UserEntity userEntity = userRepository.getById(user.getId());
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            userRepository.save(userEntity);

            LOGGER.info("User updated successfully!: {}", userEntity.getName());

            return new ResponseEntity<>(
                    "User updated successfully!",
                    HttpStatus.OK
            );
        } catch (Exception e) {
            LOGGER.error("Error updating user: {}", e.getMessage());
            return new ResponseEntity<>(
                    "Something went wrong!",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

}
