package com.userservice.UserService.controllers;

import com.userservice.UserService.dtos.OrderDTO;
import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return userService.getAllUsers();
    }

    /*
    * ===============================================
    * This method is used to return all user oder data
    * ===============================================
     */
    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id){
        return userService.getOrderesByUserId(id);
    }

    @PostMapping("/save")
    public boolean authenticateUserByPost(@RequestBody Map<String, String> data){
        UserDTO user = new UserDTO(data.get("name"), data.get("age"));
        return userService.saveUser(user);
    }

    @PostMapping("/update")
    public ResponseEntity<String> updateUser(@Valid @RequestBody UserDTO data){
        return userService.updateUser(data);
    }
}
